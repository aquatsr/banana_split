"""
Date: March 12-13, 2019
Description: Produce a test interface for interacting with the given endpoint.
             Given endpoint implements an API for solving "Bob's Banana Budget"
             problem as presented by CurrencyCap
Author: tsrawat
"""
import datetime
import json
import logging
import pytest
import requests

# Set up logging so that any modules using this can
# also utilize and see messages from this module
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

# For logging to console
console_handler = logging.StreamHandler()
console_handler.setLevel(logging.DEBUG)

console_fmt = logging.Formatter(
        '%(asctime)s - %(name)s - %(levelname)s - %(message)s',
        datefmt = '%m-%d-%Y %H:%M:%S'
        )
console_handler.setFormatter(console_fmt)

logger.addHandler(console_handler)

# For logging to file
file_handler = logging.FileHandler(filename = 'bobs_bananas.log', mode = 'a')
file_handler.setLevel(logging.DEBUG)

file_fmt = logging.Formatter(
        '%(asctime)s - %(levelname)s - %(message)s',
        datefmt = '%m-%d-%Y %H:%M:%S'
        )
file_handler.setFormatter(file_fmt)

logger.addHandler(file_handler)


# Keep how many days are in each month
# Unfortunately July and August are consecutive
# and they both have 31 days >.>
# The zeroth index has a 0 just to make the indexing
# a little cleaner
month_array = [0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]


def BananaCostGenerator(start_date, number_of_days, leap_year):
    """Given the parameters ->
    start_date :: datetime.datetime
    number_of_days :: int
    leap_year :: boolean

    This generator produces the appropriate cost of a banana for each day,
    upto and including the last day given by number_of_days.
    The generator also takes into account if the day for which the calculation
    is being made is a weekend and if so, presents $0.00 as the cost since
    Bob does not go buy bananas on the weekends.

    Note that a design decision was made here to simply construct the date from
    given information within the generator instead of attempting to 'track' or
    'calculate' whether the day was a weekend or not. Although this could
    incur a performance penalty, the resulting code is much cleaner and easier to
    understand and maintain, which is a desirable quality in reusable code"""
    def days_in_month(month):
        if month == 2 and leap_year:
            return 29
        else:
            return month_array[month]

    def it_is_a_weekend(month, day, year):
        _date = datetime.datetime.strptime('{}-{}-{}'.format(month, day, year), '%m-%d-%Y')
        return (_date.weekday() == 5) or (_date.weekday() == 6)

    # The way this works is while the number of days elapsed (generated) is still less than
    # the total number of days the user requested, return the calculated banana cost for
    # the current day/month/year step that the generator is on.
    #
    # Considerations here are that since we're guaranteed in the specs of the assignment
    # that the user won't enter a number_of_days > 365, the algorithm doesn't need to
    # readjust for leap-years...
    #
    # If the current year isn't a leap year but next year is, then 365 days takes the
    # computation from Feb 28 to Feb 28 of the next year
    # If the current year is a leap year (and the next year isn't of course), then 365
    # days takes the computation from, say, Feb 29 to Feb 28.
    # In either case there is no "overlap" into the 29th day if there is a "whole-year"
    # calculation
    #
    # Additional considerations are to honor the requirement that weekends are not 
    # viable purchase days. Hence any bananas "not to be" purchased on days determined
    # to be weekends are zeroed out.
    # 
    # The algorithm keeps track of where in the month the current day is, and checks to
    # ensure that if the days_elapsed is still less than number_of_days yet day > days in 
    # the month that the month is incremented (unless month was the last month, then roll
    # over to 1) and that day is reset to 1. In this way the Generator is able to 
    # retain state going from one month to the next and we don't need to build a Generator
    # that 'resets' (possible, but tricky to get right... simpler is usually better)
    #
    # Once the calculation for that day is produced, the Generator will increment the day
    # as well as the total days computed when computing the next iteration.
    #
    # We can take advantage of generator expressions to quickly calculate the total cost
    # for a budget given a starting date, number of days (and the leap-year information):
    #
    # E.g.
    # >>> sum(x for x in BananaCostGenerator(starting_date, number_of_days, leap_year))
    #
    day = start_date.day
    month = start_date.month
    year = start_date.year
    day_month_count = days_in_month(month)
    days_elapsed = 0
    while days_elapsed < number_of_days:
        if day > day_month_count:
            day = 1
            month += 1
            if month > 12:
                month = 1
                year += 1
            day_month_count = days_in_month(month)
        if it_is_a_weekend(month, day, year):
            yield 0.00
        else:
            yield (((day - 1) // 7) + 1) * 0.05
        day += 1
        days_elapsed += 1


class DateCheck(object):
    """Given a string this class will check if it's in the proper format
    mm-dd-yyyy and also whether it is a 'legal' date. Note that the 
    datetime module has some pretty robust error checking for valid dates.
    This class uses that to its advantage"""
    def __init__(self, date_string):
        self.data = date_string
        self.valid_date = True
        self.is_leap_year = False

        # If date matches expected format, check if it is even a 'legal' date
        try:
            self._date = datetime.datetime.strptime(self.data, '%m-%d-%Y')
        except ValueError:
            logger.error("Invalid or badly formatted start date")
            self.valid_date = False
        else:
            # If the date is valid save whether this it is in a leap-year
            self.is_leap_year = self._check_leap_year()

    def __enter__(self):
        if self.valid_date:
            logger.info("Using start date: %s", self._date.strftime('%m-%d-%Y'))
        else:
            logger.warning("Could not set valid start date. Check input data: %s", self.data)
        return self

    def __exit__(self, type, value, traceback):
        if self.valid_date:
            logger.info("Date Checks Passed")
        else:
            logger.warning("Date Checks Failed... Check Date: %s", self.data)

    def _check_leap_year(self):
        year = self._date.year
        # if the year is NOT evenly divisble by 4, it is NOT a leap year
        if year % 4:
            is_leap_year = False
        # else if the year is NOT evenly divisble by 100, it's a leap year
        elif year % 100:
            is_leap_year = True
        # else if the year is NOT evenly divisble by 400, it's NOT a leap year
        elif year % 400:
            is_leap_year = False
        # otherwise, it's a leap year
        else:
            is_leap_year = True
        return is_leap_year

    def get_date(self):
        return self._date


# Using Pytest classes for organizing the tests all in one place
# Using requests to issue GET requests to API
class TestBobsBananaBudgetApi(object):
    """Contains the test suites for testing API located at
    https://bananabudget.azurewebsites.net/?startDate=02-01-2019&numberOfDays=28
    """
    def run_request_and_calc(self, start_date, number_of_days):
        api_result = -1  # unequal defaults
        gen_result = -2
        with DateCheck(start_date) as d:
            payload = {"startDate": start_date, "numberOfDays": number_of_days}
            response = requests.get("https://bananabudget.azurewebsites.net", params = payload, timeout = 1.0)
            response_json = response.json()
            if 'error' in response_json.keys():
                api_result = response_json['error']
            else:
                api_result = response_json['totalCost'].replace('$', '')

            try:
                gen_result = "{0:2f}".format(
                    sum(x for x in BananaCostGenerator(d.get_date(), int(number_of_days), d.is_leap_year))
                    )
            except AttributeError:
                gen_result = "Invalid startDate"
        return api_result, gen_result

    def test_jan_2019_cost(self):
        """Start Date of Jan 01, 2019 with days spanning the entire month"""
        from_api, from_generator = self.run_request_and_calc('01-01-2019', '31')
        assert float(from_api) == float(from_generator), "API and Generator computations for January 2019 do not match"

    def test_feb_2019_cost(self):
        """Start Date of Feb 01, 2019 with days spanning the entire month"""
        from_api, from_generator = self.run_request_and_calc('02-01-2019', '28')
        assert float(from_api) == float(from_generator), "API and Generator computations for February 2019 do not match"

    def test_mar_2019_cost(self):
        """Start Date of Mar 01, 2019 with days spanning the entire month"""
        from_api, from_generator = self.run_request_and_calc('03-01-2019', '31')
        assert float(from_api) == float(from_generator), "API and Generator computations for March 2019 do not match"

    def test_apr_2019_cost(self):
        """Start Date of Apr 01, 2019 with days spanning the entire month"""
        from_api, from_generator = self.run_request_and_calc('04-01-2019', '30')
        assert float(from_api) == float(from_generator), "API and Generator computations for April 2019 do not match"

    def test_may_2019_cost(self):
        """Start Date of May 01, 2019 with days spanning the entire month"""
        from_api, from_generator = self.run_request_and_calc('05-01-2019', '31')
        assert float(from_api) == float(from_generator), "API and Generator computations for May 2019 do not match"

    def test_jun_2019_cost(self):
        """Start Date of Jun 01, 2019 with days spanning the entire month"""
        from_api, from_generator = self.run_request_and_calc('06-01-2019', '30')
        assert float(from_api) == float(from_generator), "API and Generator computations for June 2019 do not match"

    def test_jul_2019_cost(self):
        """Start Date of Jul 01, 2019 with days spanning the entire month"""
        from_api, from_generator = self.run_request_and_calc('07-01-2019', '31')
        assert float(from_api) == float(from_generator), "API and Generator computations for July 2019 do not match"

    def test_aug_2019_cost(self):
        """Start Date of Aug 01, 2019 with days spanning the entire month"""
        from_api, from_generator = self.run_request_and_calc('08-01-2019', '31')
        assert float(from_api) == float(from_generator), "API and Generator computations for August 2019 do not match"

    def test_sep_2019_cost(self):
        """Start Date of Sept 01, 2019 with days spanning the entire month"""
        from_api, from_generator = self.run_request_and_calc('09-01-2019', '30')
        assert float(from_api) == float(from_generator), "API and Generator computations for September 2019 do not match"

    def test_oct_2019_cost(self):
        """Start Date of Oct 01, 2019 with days spanning the entire month"""
        from_api, from_generator = self.run_request_and_calc('10-01-2019', '31')
        assert float(from_api) == float(from_generator), "API and Generator computations for October 2019 do not match"

    def test_nov_2019_cost(self):
        """Start Date of Nov 01, 2019 with days spanning the entire month"""
        from_api, from_generator = self.run_request_and_calc('11-01-2019', '30')
        assert float(from_api) == float(from_generator), "API and Generator computations for November 2019 do not match"

    def test_dec_2019_cost(self):
        """Start Date of Dec 01, 2019 with days spanning the entire month"""
        from_api, from_generator = self.run_request_and_calc('12-01-2019', '31')
        assert float(from_api) == float(from_generator), "API and Generator computations for December 2019 do not match"

    def test_weekend_purchase_cost_zero(self):
        """Test input for Feb 02, 2019 which is a Saturday and a count of 2 days"""
        from_api, from_generator = self.run_request_and_calc('02-02-2019', '2')
        assert float(from_api) == float(from_generator), "Weekend computation is non-zero"

    def test_non_existant_day_feb_29_2019(self):
        """Feb 29, 2019 is a non-existant day so the API should return an error"""
        from_api, from_generator = self.run_request_and_calc('02-29-2019', '1')
        assert from_api == 'Invalid startDate', "API doesn't validate single-day leap-year format in non-leap years"

    def test_multi_month_calc_nov_dec(self):
        """Check whether the API calculates multiple months correctly (not including Feb)... 
        try Nov (30 days) to Dec (31 days, non inclusive of all dates)"""
        from_api, from_generator = self.run_request_and_calc('11-01-2019', '50')
        assert float(from_api) == float(from_generator), "Nov -> Dec Multimonth Computations do not match"

    def test_multi_month_calc_oct_nov(self):
        """Check whether the API calculates multiple months correctly (not including Feb)... 
        try Oct (31 days) to Nov (30 days, non inclusive of all dates)"""
        from_api, from_generator = self.run_request_and_calc('10-01-2019', '50')
        assert float(from_api) == float(from_generator), "Oct -> Nov Multimonth Computations do not match"

    def test_multi_month_calc_jul_aug(self):
        """July and August both have 31 days. In 2019 the total cost should be $6.25"""
        from_api, from_generator = self.run_request_and_calc('10-01-2019', '62')
        assert float(from_api) == float(from_generator), "Oct -> Nov Multimonth Computations do not match"

    def test_non_leap_year(self):
        """Check if computation for Jan 01, 2019 365 days matches expected"""
        from_api, from_generator = self.run_request_and_calc('01-01-2019', '365')
        assert float(from_api) == float(from_generator), "2019 whole year computation doesn't match"

    def test_leap_year(self):
        """Check if computation for Jan 01, 2020 (a leap year) matches expected"""
        from_api, from_generator = self.run_request_and_calc('01-01-2020', '365')
        assert float(from_api) == float(from_generator), "2020 (a leap year) whole year computation doesn't match"

    def test_illegal_date(self):
        """Test whether the API accepts completely incorrect dates"""
        from_api, from_generator = self.run_request_and_calc('01-35-2019', '1')
        assert from_api == from_generator, "API accepts incorrect dates"

    def test_leap_year_calc(self):
        """Check if within a leap-year the API has proper calculation... expected $0.45"""
        from_api, from_generator = self.run_request_and_calc('02-27-2020', '5')
        assert float(from_api) == float(from_generator), "API unable to handle multi-month leap year calculations properly"


