# banana_split

An interface for testing the "Bob's Banana Budget" service endpoint

## Installation

This project relies on Python 3.6.5. The quickest way to support this environment is to utilize a Python installation manager (like Pyenv) and also take advantage of Python Virtual Environments. First install pyenv (and perhaps, update your PATH variable). Next perform the following operations:

1. `pyenv install 3.6.5`
2. `git clone https://aquatsr@bitbucket.org/aquatsr/banana_split.git .`
3. `cd banana_split`
4. `pyenv local 3.6.5`
5. `pyenv global 3.6.5`
6. `virtualenv -p $(pyenv which python) venv`
7. `. venv/bin/activate`
8. `pip install -r requirements.txt`

Once the environment is configured, run the test suite by invoking `pytest`:

    pytest banana_api.py

Note that one test is expected to fail, while the rest should pass (20/21 pass)

## Bugs Found

1. When providing a date valid for a leap-year in a non-leap year and given a single day for the calculation. The API accepts this incorrect date and presents a cost calculation instead of returning an error. Note that the DateCheck class within `banana_api.py` is able to catch the faulty date and does not allow computation to proceed.

## Logging

For convenience logs are both written to console (captured by pytest when running tests) and also written to a log file `bobs_bananas.log`. If the log file gets too large, simply archive it or rename it, a new log file will automatically be created.

